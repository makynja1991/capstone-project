﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Capstone.Data.Migrations
{
    /// <inheritdoc />
    public partial class RemoveIsCompleted : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "ToDoItems");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "ToDoItems",
                type: "bit",
                nullable: true);
        }
    }
}
