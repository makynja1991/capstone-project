﻿using Capstone.Data.Enums;
using System.ComponentModel.DataAnnotations;

namespace Capstone.Web.Data.Enums
{
    public static class Extensions
    {
        public static string GetDisplayName(Enum value)
        {
            var type = value.GetType();
            var members = type.GetMember(value.ToString());
            if (members.Length > 0)
            {
                var attrs = members[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                if (attrs.Length > 0)
                {
                    return ((DisplayAttribute)attrs[0]).GetName();
                }
            }
            return value.ToString();
        }

        public static string GetFontAwesomeIcon(this Status status)
        {
            switch (status)
            {
                case Status.NotStarted:
                    return "fas fa-circle";
                case Status.InProgress:
                    return "fas fa-hourglass-half";
                case Status.Completed:
                    return "fas fa-check-circle";
                default:
                    return string.Empty;
            }
        }
    }
}
