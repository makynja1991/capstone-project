﻿using Microsoft.AspNetCore.Mvc;
using Capstone.Data;
using Microsoft.EntityFrameworkCore;

namespace Capstone.Controllers
{
    public class PlannerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlannerController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.ToDoLists.Include(t => t.Items).ToListAsync());
        }
        public async Task<IActionResult> Today()
        {
            var todayItems = await _context.ToDoLists
                                           .Include(t => t.Items)
                                           .Where(t => t.Items.Any(i => i.DueDate.Date == DateTime.Now.Date))
                                           .ToListAsync();
            return View(todayItems);
        }

        public async Task<IActionResult> Planned()
        {
            var startDate = DateTime.Now.Date;
            var endDate = startDate.AddDays(7);

            var plannedItems = await _context.ToDoLists
                                             .Include(t => t.Items)
                                             .Where(t => t.Items.Any(i => i.DueDate.Date >= startDate && i.DueDate.Date <= endDate))
                                             .ToListAsync();
            return View(plannedItems);
        }

    }
}
