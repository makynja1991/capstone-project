﻿using Microsoft.AspNetCore.Mvc;
using Capstone.Data;
using Capstone.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Capstone.Data.Enums;


namespace Capstone.Controllers
{
    public class ToDoController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ToDoController> _logger;


        public ToDoController(ApplicationDbContext context, ILogger<ToDoController> logger)
        {
            _context = context;
            _logger = logger;
        }


        public async Task<IActionResult> Index()
        {
            var uncompletedTasksCount = _context.ToDoItems.Count();

            return View(await _context.ToDoLists.Include(t => t.Items).ToListAsync());
        }

        public async Task<IActionResult> Planner()
        {
            return View(await _context.ToDoLists.Include(t => t.Items).ToListAsync());
        }

        public IActionResult View(int id)
        {
            var toDoList = _context.ToDoLists
                                   .Include(t => t.Items)
                                   .SingleOrDefault(t => t.Id == id);
            if (toDoList == null)
            {
                return NotFound();
            }

            return View(toDoList);
        }

        // GET: ToDo/CreateList
        public IActionResult Create()
        {
            return View();
        }

        // POST: ToDo/CreateList
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name")] ToDoList toDoList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(toDoList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(toDoList);
        }

        // GET: ToDo/EditList/5
        public async Task<IActionResult> EditList(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoList = await _context.ToDoLists.FindAsync(id);
            if (toDoList == null)
            {
                return NotFound();
            }
            return View("Edit", toDoList);
        }

        // POST: ToDo/EditList/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditList(int id, [Bind("Id,Name")] ToDoList toDoList)
        {
            if (id != toDoList.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toDoList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoListExists(toDoList.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(toDoList);
        }

        // GET: ToDo/DeleteList/5
        public async Task<IActionResult> DeleteList(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoList = await _context.ToDoLists
                .Include(t => t.Items)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoList == null)
            {
                return NotFound();
            }

            return View(toDoList);
        }

        // POST: ToDo/DeleteList/5
        [HttpPost, ActionName("DeleteList")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteListConfirmed(int id)
        {
            var toDoList = await _context.ToDoLists.FindAsync(id);
            _context.ToDoLists.Remove(toDoList);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToDoListExists(int id)
        {
            return _context.ToDoLists.Any(e => e.Id == id);
        }

        // GET: ToDo/CreateItem
        public IActionResult CreateItem(string currentListId)
        {
            ViewBag.CurrentListId = currentListId;
            ViewBag.ToDoListId = new SelectList(_context.ToDoLists, "Id", "Name");
            return View();
        }

        // POST: ToDo/CreateItem
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateItem([Bind("Title,Description,DueDate,CreationDate,Status,ToDoListId")] ToDoItem toDoItem)
        {            
            if (ModelState.IsValid)
            {
                _context.Add(toDoItem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewBag.ToDoListId = new SelectList(_context.ToDoLists, "Id", "Name", toDoItem.ToDoListId);
            return View(toDoItem);
        }

        // GET: ToDo/EditItem/5
        public async Task<IActionResult> EditItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoItem = await _context.ToDoItems.FindAsync(id);
            if (toDoItem == null)
            {
                return NotFound();
            }
            ViewBag.ToDoListId = new SelectList(_context.ToDoLists, "Id", "Name", toDoItem.ToDoListId);
            return View(toDoItem);
        }

        // POST: ToDo/EditItem/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditItem(int id, [Bind("Id,Title,Description,DueDate,CreationDate,Status,ToDoListId")] ToDoItem toDoItem)
        {
            if (id != toDoItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toDoItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoItemExists(toDoItem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("View", new { id = toDoItem.ToDoListId });
            }
            ViewBag.ToDoListId = new SelectList(_context.ToDoLists, "Id", "Name", toDoItem.ToDoListId);
            return View(toDoItem);
        }

        // GET: ToDo/DeleteItem/5
        public async Task<IActionResult> DeleteItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoItem = await _context.ToDoItems
                .Include(t => t.ToDoList)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoItem == null)
            {
                return NotFound();
            }

            return View(toDoItem);
        }

        // POST: ToDo/DeleteItem/5
        [HttpPost, ActionName("DeleteItem")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteItemConfirmed(int id)
        {
            var toDoItem = await _context.ToDoItems.FindAsync(id);
            _context.ToDoItems.Remove(toDoItem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> CompleteItem(int id)
        {
            var task = await _context.ToDoItems.FindAsync(id);
            if (task == null)
            {
                return NotFound();
            }

            task.Status = Status.Completed;
            _context.Update(task);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(View), new { id = task.ToDoListId });
        }

        [HttpPost]
        public async Task<IActionResult> DuplicateList(int id)
        {
            var originalList = await _context.ToDoLists
                                             .Include(t => t.Items)
                                             .SingleOrDefaultAsync(t => t.Id == id);

            if (originalList == null)
            {
                return NotFound();
            }

            // Create a new list based on the original
            var newList = new ToDoList
            {
                Name = originalList.Name + " (Copy)"  // You can modify this naming pattern as per your needs
            };

            _context.ToDoLists.Add(newList);
            await _context.SaveChangesAsync();  // Save the new list to get its Id

            // Copy items from the original list to the new list
            foreach (var item in originalList.Items)
            {
                var newItem = new ToDoItem
                {
                    Title = item.Title,
                    Description = item.Description,
                    DueDate = item.DueDate,
                    CreationDate = DateTime.Now,  // Set current date as creation date for the copied item
                    Status = item.Status,
                    ToDoListId = newList.Id
                };

                _context.ToDoItems.Add(newItem);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(View), new { id = newList.Id });
        }

        public IActionResult PrivacyPolicy()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(int toDoItemId, string commentText)
        {
            var toDoItem = await _context.ToDoItems.FindAsync(toDoItemId);
            if (toDoItem == null)
            {
                return NotFound();
            }

            var comment = new Comment
            {
                Text = commentText,
                CreationDate = DateTime.Now,
                ToDoItemId = toDoItemId
            };

            _context.Comments.Add(comment);
            await _context.SaveChangesAsync();

            return RedirectToAction("Comments", new { id = toDoItemId });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteComment(int commentId)
        {
            var comment = await _context.Comments.FindAsync(commentId);
            if (comment == null)
            {
                return NotFound();
            }

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();

            return RedirectToAction("Comments", new { id = comment.ToDoItemId });
        }

        public async Task<IActionResult> Comments(int id)
        {
            var toDoItem = await _context.ToDoItems.Include(t => t.Comments).FirstOrDefaultAsync(t => t.Id == id);
            
            if (toDoItem == null)
            {
                return NotFound();
            }

            return View(toDoItem);
        }

        private bool ToDoItemExists(int id)
        {
            return _context.ToDoItems.Any(e => e.Id == id);
        }
    }
}
