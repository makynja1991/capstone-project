# UpSkillMe_.Net ASP.NET Core 3.1 Capstone Project: ToDo Application

Live demo: https://todoappmakusha.azurewebsites.net/

## Table of Contents
1. [Introduction](#introduction)
2. [Application Description](#application-description)
3. [User Stories and Features](#user-stories-and-features)
4. [Application Architecture](#application-architecture)
5. [Unit Tests](#unit-tests)
6. [Getting Started](#getting-started)
7. [Feedback](#feedback)
8. [Contributors](#contributors)

## Introduction

Hello, I'm Darya Trafimenka, and this repository contains my capstone project for the UpSkillMe_.Net course. The project is a ToDo application designed to assist users in task and to-do list management, aiding in daily organization and efficiency.

## Application Description

The ToDo application allows users to:

- View all task lists
- Create new task lists
- Add entries to the task list with a title, description, and due date
- Change the status of tasks to: 'Completed', 'In Progress', 'Not Started'
- Edit task lists and their items

The application is optimized for desktop and mobile use, ensuring a seamless experience across various devices.

## User Stories and Features

The application provides a user-friendly interface for task management. Users can create, edit, and delete task lists, add tasks with priorities and deadlines, and even duplicate lists for similar projects. The design is minimalist, focusing on task management without unnecessary distractions. Data synchronization across devices is a key feature, enabling users to manage tasks anytime, anywhere.

## Application Architecture

The application follows the MVC model, ensuring a clear separation between data, views, and actions. It incorporates a data access layer for efficient and secure data interaction using Entity Framework Core. The architecture also includes well-defined data models and a request-response cycle ensuring fast and reliable interaction between the user and the application.

#### High level diagram
![high-level-diagram](images/high-level-diagram.png)

#### Sequence diagram
![sequence-diagram](images/sequence-diagram.png)

#### Database diagram
![database-diagram](images/database-diagram.png)

#### Layers diagram
![layers-diagram](images/layers-diagram.png)

## Unit Tests

A suite of unit tests has been implemented to ensure the reliability and robustness of the application by testing its core functionalities. These tests cover various aspects like creating, editing, deleting, and duplicating task lists, as well as adding comments to tasks.

## Feedback

Live demo: https://todoappmakusha.azurewebsites.net/

I welcome any feedback and suggestions to improve this application. Please feel free to open an issue or submit a pull request.

## Contributors

- Darya Trafimenka

---

Thank you for your attention. Your feedback is highly appreciated.

