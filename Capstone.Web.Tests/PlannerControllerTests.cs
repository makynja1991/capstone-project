﻿using Capstone.Controllers;
using Capstone.Data;
using Microsoft.EntityFrameworkCore;
using Capstone.Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace Capstone.Tests
{
    [TestClass]
    public class PlannerControllerTests
    {
        private ApplicationDbContext _context;
        private PlannerController _controller;

        [TestInitialize]
        public void Setup()
        {
            // Clean up any existing data and set up in-memory database
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())  // Use a unique name for each test run
                .Options;

            _context = new ApplicationDbContext(options);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();  // Make sure database is created

            // Seed data
            var list = new ToDoList { Name = "TestList" };
            var itemToday = new ToDoItem
            {
                Title = "TodayTask",
                DueDate = DateTime.Now
            };
            var itemFuture = new ToDoItem
            {
                Title = "FutureTask",
                DueDate = DateTime.Now.AddDays(5)
            };
            list.Items.Add(itemToday);
            list.Items.Add(itemFuture);
            _context.ToDoLists.Add(list);
            _context.SaveChanges();

            _controller = new PlannerController(_context);
        }

        [TestMethod]
        public async Task Index_ReturnsAllLists()
        {
            var result = await _controller.Index();
            var viewResult = result as ViewResult;

            Assert.IsNotNull(viewResult);
            var lists = viewResult.Model as List<ToDoList>;
            Assert.AreEqual(1, lists.Count);
        }

        [TestMethod]
        public async Task Today_ReturnsTasksForToday()
        {
            var result = await _controller.Today();
            var viewResult = result as ViewResult;

            Assert.IsNotNull(viewResult);
            var lists = viewResult.Model as List<ToDoList>;
            Assert.AreEqual(1, lists.Count);
            Assert.AreEqual(1, lists.First().Items.Count(i => i.DueDate.Date == DateTime.Now.Date));
        }

        [TestMethod]
        public async Task Planned_ReturnsTasksForNext7Days()
        {
            var result = await _controller.Planned();
            var viewResult = result as ViewResult;

            Assert.IsNotNull(viewResult);
            var lists = viewResult.Model as List<ToDoList>;
            Assert.AreEqual(1, lists.Count);
            Assert.AreEqual(2, lists.First().Items.Count);
        }
    }
}
