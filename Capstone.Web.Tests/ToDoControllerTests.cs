﻿using Capstone.Controllers;
using Capstone.Data;
using Microsoft.EntityFrameworkCore;
using Capstone.Data.Models;
using Microsoft.Extensions.Logging;
using Moq;
using Capstone.Data.Enums;
using Microsoft.AspNetCore.Mvc;

namespace Capstone.Tests
{
    [TestClass]
    public class ToDoControllerTests
    {
        private ApplicationDbContext _context;
        private ToDoController _controller;
        private Mock<ILogger<ToDoController>> _mockLogger;

        [TestInitialize]
        public void Setup()
        {
            // Setup mock logger
            _mockLogger = new Mock<ILogger<ToDoController>>();

            // Set up in-memory database
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;
            _context = new ApplicationDbContext(options);

            _controller = new ToDoController(_context, _mockLogger.Object);
        }

        [TestMethod]
        public async Task Create_NewToDoList()
        {
            var list = new ToDoList { Name = "NewTestList" };
            var result = await _controller.Create(list) as RedirectToActionResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ActionName);
            Assert.IsTrue(_context.ToDoLists.Any(l => l.Name == "NewTestList"));
        }

        [TestMethod]
        public async Task Edit_ExistingToDoList()
        {
            var list = new ToDoList { Name = "TestList" };
            _context.ToDoLists.Add(list);
            await _context.SaveChangesAsync();

            list.Name = "UpdatedTestList";
            var result = await _controller.EditList(list.Id, list) as RedirectToActionResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ActionName);
            Assert.IsTrue(_context.ToDoLists.Any(l => l.Name == "UpdatedTestList"));
        }

        [TestMethod]
        public async Task Delete_ExistingToDoList()
        {
            var list = new ToDoList { Name = "TestList" };
            _context.ToDoLists.Add(list);
            await _context.SaveChangesAsync();

            var result = await _controller.DeleteListConfirmed(list.Id) as RedirectToActionResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ActionName);
            Assert.IsFalse(_context.ToDoLists.Any(l => l.Name == "TestList"));
        }

        [TestMethod]
        public async Task Complete_ExistingToDoItem()
        {
            var item = new ToDoItem { Title = "TestItem", Status = Status.NotStarted };
            _context.ToDoItems.Add(item);
            await _context.SaveChangesAsync();

            var result = await _controller.CompleteItem(item.Id) as RedirectToActionResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("View", result.ActionName);
            Assert.IsTrue(_context.ToDoItems.Any(i => i.Status == Status.Completed));
        }

        [TestMethod]
        public async Task Duplicate_ExistingToDoList()
        {
            var list = new ToDoList { Name = "TestList" };
            _context.ToDoLists.Add(list);
            await _context.SaveChangesAsync();

            var result = await _controller.DuplicateList(list.Id) as RedirectToActionResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("View", result.ActionName);
            Assert.IsTrue(_context.ToDoLists.Any(l => l.Name == "TestList (Copy)"));
        }

        [TestMethod]
        public async Task AddComment_ToToDoItem()
        {
            var item = new ToDoItem { Title = "TestItem" };
            _context.ToDoItems.Add(item);
            await _context.SaveChangesAsync();

            var result = await _controller.AddComment(item.Id, "TestComment") as RedirectToActionResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Comments", result.ActionName);
            Assert.IsTrue(_context.Comments.Any(c => c.Text == "TestComment"));
        }
    }
}
